import React, { Component } from 'react';
// Tengo que wrapear en este componente, toda la parte de la APP que va a tener routing. Generalmente se hace en el App.js o el index.js
import { BrowserRouter } from 'react-router-dom';

import Blog from './containers/Blog/Blog';


class App extends Component {
  render() {
    return (
      // Agregando la prop "basename" al BrowserRouter, le seteo el root, por default es  "/", pero si le pusiera "/hola", mi app arrancaria corriendo
      //  en "localhost:3000/hola".
      <BrowserRouter> 
        <div className="App">
          <Blog />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
