import axios from 'axios';

/*
    Usando esta idea de las instancias puedo flexibilizar la configuracion y que cada parte de mi app llame a una distinta.
    En mi ejemplo solo uso esta instancia en el Blog.js para traer todos los posts y sin interceptors.
    Lo ideal seria tener siempre este archivo, asi no ensucio el "index.js".
*/

const instance = axios.create({
    baseURL : 'https:/jsonplaceholder.typicode.com'
});

instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

// instance.interceptors.request...

export default instance;