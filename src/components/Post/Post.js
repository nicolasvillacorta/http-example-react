import React from 'react';
// import { withRouter } from 'react-router-dom';

import './Post.css';

const post = (props) => (
    <article className="Post" onClick={props.clicked} >
        <h1>{props.title}</h1>
        <div className="Info">
            <div className="Author">{props.author}</div>
        </div>
    </article>
);

export default post;
// Usando el componente withRouter y exportando este componente hijo del componente Posts, el cual recibe las props de routing, puedo hacer que este tambien
//  reciba las props del routeo, por default los components hijos no reciben las props de routing. (A menos que las pase a mano, o para evitar pasarlas a mano,
//  use este componente hoc. En este caso lo comento porque no me interesa hacerlo).
// export default withRouter(post);