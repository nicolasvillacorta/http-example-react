import React, { Component } from 'react';
import './Blog.css';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';
import Posts from './Posts/Posts';
import asyncComponent from '../../hoc/asyncComponent';

/*
    IMPORTANTE: Cuando yo importo el NewPost, el usuario apenas entra en el roth, como carga este componente, se descarga en cliente todos los imports.
        Haciendo este HOC y llamando mediante el al NewPost, hago que el cliente descargue el componente NewPost cuando lo llama, es decir si lo necesita.
        En este caso es solo un archivo de 3kb que me estoy ahorrando, pero en apps grandes pueede ser algo super util. (Cuando llamo al NewPost en el route,
        llame al AsyncNewPost al final.) Esto se llama Lazy Loading.
*/
// import NewPost from './NewPost/NewPost';
const AsyncNewPost = asyncComponent( () => {
    return import('./NewPost/NewPost');
});

/* --- A partir de React 16.6, puedo hacer el lazy loading de la siguiente manera sin crear un componente yo; 

    1)De esta manera declaro el componente:
        const NewPost = React.lazy( () => import('./NewPost/NewPost'));
    2) De esta manera muestro la ruta, dentro del componente Suspense, y el fallback es lo que se muestra mientras se tarda en cargar la respuesta del componente, puede ser un spinner, etc.
        <Route path="/new-post" render={ () => (
            <Suspense fallback={<div>Loading...</div>}><NewPost/></Suspense>
        )} />
*/

class Blog extends Component {

    state = {
        auth: true
    }
    render () {

        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            {/*Con el prop "to", ya funciona el pathing, pero como en este ejemplo, puedo usar un objeto de JS para tener mas propiedades,
                                Siempre que uso react-router en lugar de usar el tag 'a', se deben usar los 'Link', sino la pagina se reloadea cada vez que
                                cambio de un link a otro, de esta manera nunca se pierde el estado.
                                NavLink es como Link pero agrega posibilidad de darle estilo. Ahora el boton marcado va a tenes la clase "active", solamente
                                le agrego el estilo en el Blog.css y listo. Tuve que agregar el exact tambien, porque sino el home iba a ser siempre
                                activo. (pintado) */}
                            <li><NavLink 
                                    to="/posts" 
                                    exact 
                                    /*activeClassName="my-active" [CON ESTA PROP PUEDO PONERLE UNA CLASE DE CSS CUSTOM, QUE NO SE ACTIVE.]
                                    activeStyle={{ [CON ESTA PROP PUEDO PONERLE STYLE ACA MISMO]
                                        color: '#fa923f',
                                        textDecoration: 'underlined'
                                    }}*/>Posts</NavLink></li>
                            <li><NavLink to={{
                                // pathname : this.props.match.url + '/new-post', Asi usa un path relativo.
                                pathname: "/new-post",  // Asi usa un path absoluto, este va a ir SIEMPRE a "localhost:3000/new-post" sin importar de que componente lo llame.
                                hash: '#submit',
                                search: '?quick-submit=true'
                            }} >New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>
                {/* El boolean exact, significa que matchee el path perfecto, y no como prefijo. La prop render, renderiza JSX, para componentes uso component.*/}
                {/*<Route path="/" exact render={() => <h1>Home</h1>} /> 
                <Route path="/" render={() => <h1>Home</h1>} />
                    Gracias al Switch, solo se va a renderizar un componente, sino, tras renderizar a uno, si la ruta matchea con otro quedan los 2 renderizados*/}
                <Switch> 
                    {this.state.auth ?  <Route path="/new-post" component={AsyncNewPost} /> : null} { /*De esta forma evito que si no esta autenticado no pueda ir a esta ruta. (Conocido como navigations guards)*/}
                    {/* Esta linea tiene que estar abajo, sino new-post va a entrar como id. El orden es importante en las rutas.*/}
                    <Route path="/posts" component={Posts} /> {/* Al final le saque el exact, porque sino no matchea con la nested route, y al agregar el id, deja de matchear con este */}
                    {/*<Route render={() => <h1>Not found</h1>} / > (No tiene "path". Con esta linea le digo, que si no matchea ninguna ruta, renderize esto. */}
                    <Redirect from="/" to="/posts" /> {/* Si uso el redirect fuera de un switch, no lleva el prop 'from', solo el 'to' */}
                </Switch>
            </div>
        );
    }
}

export default Blog;