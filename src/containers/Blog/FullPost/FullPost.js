import React, { Component } from 'react';
import axios from 'axios';

import './FullPost.css';

class FullPost extends Component {

    state = {
        loadedPost: null
    }

    /* CADA VEZ QUE SETEO EL ESTADO, EL COMPONENTE SE UPDATEA, POR LO TANTO SE REEJECUTA EL COMPONENTDIDUPDATE.
        PARA ESO ME ASEGURO QUE YA SE CARGO UN POST PRIMERO. */
//  componentDidUpdate() { AHORA CON EL ROUTING, VA A SER COMPONENTDIDMOUNT, PORQUE EL COMPONENTE SE RENDERIZA Y REMUEVE, NO SE UPDATEA. 
    componentDidMount() {
        console.log(this.props);
        this.loadData();
    }
    
    // Tuve que agregar este metodo ahora porque sino despues de las nested routes, el componente nunca se renovava a menos que vuelva atras.
    componentDidUpdate() {
        this.loadData();
    }

    loadData() {
        if(this.props.match.params.id){
            if(!this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== + this.props.match.params.id)){ // Le agrege el '+' para que convierta en numero el string (id) que viene de params.id
                axios.get('https://jsonplaceholder.typicode.com/posts/' + this.props.match.params.id)
                .then(response => {
                    // console.log(response);
                    this.setState({loadedPost: response.data}) // Al llamar este metodo, el componente se updatea --> se ejecuta denuevo el componenteDidUpdate.
                });   
            }
            
        }
    }

    deletePostHandler = () => {
        axios.delete('/posts/' + this.props.match.params.id)
            .then( response => {
                console.log(response);
            });
    }

    render () {
        let post = <p style={{textAlign: 'center'}}>Please select a Post!</p>;
        
        if(this.props.match.params.id){
            post = <p style={{textAlign: 'center'}}>Loading...!</p>;
        }
        // Por default al pasar null, el if lo trata como false.
        if(this.state.loadedPost) {
            post = (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button className="Delete" onClick={this.deletePostHandler}>Delete</button>
                    </div>
                </div>
    
            );
        }
        
        return post;
    }
}

export default FullPost;