import React, { Component } from 'react';
import axios from '../../../axios';
import Post from '../../../components/Post/Post';
import FullPost from '../FullPost/FullPost';
import './Posts.css'
import { Link, Route } from 'react-router-dom';

class Posts extends Component {

    state = {
        posts: []
    }

    componentDidMount() {
        console.log(this.props);
        
        // Por default estas llamadas son asincronas. Then() toma una funcion como input y esa funcion se ejecuta cuando la promise se resuelva, es decir
        // cuando venga la data del llamado. No se debe asignar el axios... a una const, porque en la linea siguiente se va a bloquear el codigo, porque la 
        // llamada es asincrona.
        axios.get('/posts')  
            .then( response => {
                const posts = response.data.slice(0, 4); // Solo agarra los primeros 4 posts.
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: 'Niquin'
                    }
                })
                // Siempre debe estar dentro del metodo then el this.setState, si estuviera despues el codigo aun no tendria el resultado asinc, 
                // y a diferencia de Java, no espera la respuesta, sino que va a asignar nada.
                this.setState({posts: updatedPosts})
                 console.log(response);
            })
            .catch( error => { // Este si logea el error del GET con mal url.
                //console.log(error);
                this.setState({error: true});
            });
    }

    postSelectedHandler = (id) => {
        this.setState({selectedPostId: id});
    }

    render() {

        let posts = <p style={{textAlign: 'center'}}>Something went wrong!</p>
        if(!this.state.error){
            posts = this.state.posts.map( post => {
                return (
                    <Link to={'/posts/' + post.id} key={post.id}> {/* Esto va a ser la salida de un link, asi que lo wrappeo, y como siempre, la key tiene que estar en el componente padre del JSX, la pase aca.*/}
                        <Post     
                            title={post.title} 
                            author={post.author}
                            clicked={ () => this.postSelectedHandler(post.id) } />
                    </Link> )
            });
        }

        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <Route path={this.props.match.url + "/:id"} exact  component={FullPost} />
            </div>
        )
    }

}

export default Posts;