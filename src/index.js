import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// Interceptors, para ejecutar algo cada vez que se recibe o se envia una request.
import axios from 'axios';

// Seteo la baseURL para hacer mas simples los get en los demas componentes.
axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';

/*
----PUEDO SETEAR POR EJEMPLO QUE SIEMPRE LOS HEADERS LLEVEN LA AUTHORIZATION, O EL CONTENT TYPE (EN ESTE CASO YA ES JSON POR DEFAULT)
axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN';
axios.defaults.headers.post['Content-Type'] = 'application/json'; */



// Con esta linea ya aplique los interceptors en toda la app.
axios.interceptors.request.use(request => {
    console.log(request);
    // La idea es editar la request aca.
    return request; // Si no retorno la request, se bloquea.
}, error => { 
    // Si pongo mal la URL de algun GET este no logea nada, porque logea si hay problemas al enviar la request, por ejemplo si no tengo internet (esto decia el video)
    console.log(error);
    return Promise.reject(error);
});

axios.interceptors.response.use( response => {
    console.log(response);
    // Aca edito la response
    return response;
}, error => {
    console.log(error);
    return Promise.reject(error);
});

ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();

/*
------- PARA PODER REMOVER UN INTERCEPTOR, LO GUARDO EN UNA VARIABLE Y LUEGO LLAMO EL EJECT CON ESA VARIABLE DE ARGUMENTO.
var myInterceptor = axios.interceptors.request.use(function () {...});
axios.interceptors.request.eject(myInterceptor);
*/